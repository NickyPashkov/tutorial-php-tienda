<?php

class Products {

  private $db;

  public function __construct($db) {
    $this->db = $db;
  }

  public function index() {
    $products = $this->db->query("SELECT * FROM products");

    return new Template("views/products/index.html", [
      "products" => $products
    ]);
  }

  public function show($id) {
    $id = $this->db->escape($id);
    $product = $this->db->queryOne("SELECT * FROM products WHERE product_id = ".$id);

    return new Template("views/products/show.html", [
      "product" => $product
    ]);
  }

  public function create() {
    # code...
  }

  public function update($id) {
    # code...
  }

  public function delete($id) {
    # code...
  }
}